@extends('layouts.admin._master-admin')
@section('content')

<!-- Content -->
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Master /</span> Admin</h4>

    <div class="row g-4 mb-4">
        <div class="col-sm-6 col-xl-3">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="avatar">
                            <div class="avatar-initial bg-label-primary rounded">
                                <div class="mdi mdi-account-outline mdi-24px"></div>
                            </div>
                        </div>
                        <div class="ms-3">
                            <div class="d-flex align-items-center">
                                <h5 class="mb-0">8,458</h5>
                                <div class="mdi mdi-chevron-down text-danger mdi-24px"></div>
                                <small class="text-danger">8.1%</small>
                            </div>
                            <small class="text-muted">New Customers</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="avatar">
                            <div class="avatar-initial bg-label-warning rounded">
                                <div class="mdi mdi-poll mdi-24px"></div>
                            </div>
                        </div>
                        <div class="ms-3">
                            <div class="d-flex align-items-center">
                                <h5 class="mb-0">$28.5K</h5>
                                <div class="mdi mdi-chevron-up text-success mdi-24px"></div>
                                <small class="text-success">18.2%</small>
                            </div>
                            <small class="text-muted">Total Profit</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="avatar">
                            <div class="avatar-initial bg-label-info rounded">
                                <div class="mdi mdi-trending-up mdi-24px"></div>
                            </div>
                        </div>
                        <div class="ms-3">
                            <div class="d-flex align-items-center">
                                <h5 class="mb-0">2,450K</h5>
                                <div class="mdi mdi-chevron-down text-danger mdi-24px"></div>
                                <small class="text-danger">24.6%</small>
                            </div>
                            <small class="text-muted">New Transaction</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="avatar">
                            <div class="avatar-initial bg-label-success rounded">
                                <div class="mdi mdi-currency-usd mdi-24px"></div>
                            </div>
                        </div>
                        <div class="ms-3">
                            <div class="d-flex align-items-center">
                                <h5 class="mb-0">$48.2K</h5>
                                <div class="mdi mdi-chevron-down text-success mdi-24px"></div>
                                <small class="text-success">22.5%</small>
                            </div>
                            <small class="text-muted">Total Revenue</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- DataTable with Buttons -->
    <div class="card">
        <div class="card-datatable table-responsive pt-0">
            <table class="datatables-admin table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama</th>
                        <th>Nomor Identitas</th>
                        <th>Email</th>
                        <th>No HP</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <!--/ DataTable with Buttons -->

    <div class="modal fade" id="addAdminModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content p-3 p-md-5">
                <button type="button" class="btn-close btn-pinned" data-bs-dismiss="modal"
                aria-label="Close"></button>
                <div class="modal-body p-md-0">
                    <div class="text-center mb-4">
                        <h3 class="mb-2 pb-1">Tambah Admin</h3>
                        <p>Tambah Admin</p>
                    </div>
                    <form action="{{route('master-admin.store')}}" method="POST">
                        @csrf
                        <div class="col-12 mb-3">
                            <div class="form-floating form-floating-outline">
                                <input type="text" id="nama_lengkap" name="nama_lengkap"
                                class="form-control" placeholder="Nama Lengkap"/>
                                <label>Nama Lengkap</label>
                            </div>
                        </div>

                        <div class="form-floating form-floating-outline mt-3">
                            <input type="text" id="nomor_identitas" name="nomor_identitas" class="form-control" placeholder="326xxxx"/>
                            <label>Nomor Identitas</label>
                        </div>

                        <div class="form-floating form-floating-outline mt-3">
                            <input type="email" id="email" name="email" class="form-control" placeholder="lasernarindo@gmail.com"/>
                            <label>Email</label>
                        </div>

                        <div class="form-floating form-floating-outline mt-3">
                            <input type="text" id="no_hp" name="no_hp" class="form-control" placeholder="081387xx26xx"/>
                            <label>No Handphone</label>
                        </div>

                        <div class="form-floating form-floating-outline mt-3">
                            <select class="form-control" id="role" name="role">
                                @foreach ($list_role as $item)
                                <option value="{{ $item->id }}">{{ $item->name}}</option>
                                @endforeach
                            </select>
                            <label>Role Name</label>
                        </div>

                        <div class="col-12 text-center demo-vertical-spacing">
                            <button type="submit" class="btn btn-primary btn-submit me-sm-3 me-1">Simpan</button>
                            <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                                Tutup
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>

@push('custom-scripts')
<script src="{{ URL::asset('resources/js/admin/master-admin.js') }}"></script>
@endpush

@endsection
