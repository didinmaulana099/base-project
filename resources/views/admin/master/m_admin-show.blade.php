@extends('layouts.admin._master-admin')
@section('content')

<!-- Content -->
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Master /</span> Admin Edit</h4>


</div>

@push('custom-scripts')
<script src="{{ URL::asset('resources/js/admin/admin-datatable.js') }}"></script>
@endpush

@endsection
