@extends('layouts.admin._settings')
@section('content')
    <div class="content-wrapper">
        <div class="container-xxl flex-grow-1 container-p-y">
            <!-- Role cards -->
            <div class="row g-4">
                <h4 class="fw-medium mb-1 mt-5">Pengaturan Role</h4>
                <p class="mb-0 mt-1">Temukan role kalian di kolom tabel dibawah ini!</p>

                <div class="col-12">
                    <!-- Role Table -->
                    <div class="card">
                        <div class="card-datatable table-responsive">
                            <table class="datatables-role table">
                                <thead class="table-light">
                                    <tr>
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th>Keterangan</th>
                                        <th>Created By</th>
                                        <th>Updated BY</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <!--/ Role Table -->
                </div>
            </div>
            <!--/ Role cards -->
            @include('settings.role.addModal')
            <div class="modal fade" id="editRoleModal" tabindex="-1" aria-hidden="true">
                {{-- @include('settings.role.editModal') --}}
            </div>

        </div>
    </div>

    @push('custom-scripts')
        <script src="{{ URL::asset('resources/js/admin/role.js') }}"></script>

        <script async defer src="https://buttons.github.io/buttons.js"></script>
    @endpush
@endsection
