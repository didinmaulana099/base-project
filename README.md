How to Run This Project


```bash
    Using Native Composer
    1. run `composer install`
    2. Copy .env.example to .env
    3. fill Credentials
    4. run `php artisan key:generate`

    5. if the project database is empty then run
    `php artisan migrate`

    6. to run with artisan `php artisan serve`

    7. it runs on port 8000
```


```bash
    Using Docker Compose
    1. copy .env.example to .env
    2. Fill credentials
    3. run php artisan key:generate

    4. if the project database is empty then run
    `php artisan migrate`

    5. run `docker-compose up -d`

    6. it runs on port 9000
```