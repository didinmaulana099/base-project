<?php

use Illuminate\Support\Facades\Route;

//admin
use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\PermissionController;
use App\Http\Controllers\Admin\ConfigurationController;
use App\Http\Controllers\Admin\MAdminController;
use App\Http\Controllers\Admin\MUserController;
use App\Http\Controllers\Admin\DashboardAdminController;

// user controller
use App\Http\Controllers\User\DashboardUserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// admin route
Route::get('/', [AuthController::class, 'index'])->name('auth.login');
Route::get('/login', [AuthController::class, 'index'])->name('auth.login');

Route::middleware(['has_login'])->group(function () {
    // Dashboard Admin
    Route::get('/dashboard', [DashboardAdminController::class, 'index'])->name('dashboard');
    Route::get('/dashboard-admin', [DashboardAdminController::class, 'index'])->name('dashboard.admin');

    // master Data
    Route::get('/master-admin', [MAdminController::class, 'index'])->name('master-admin.index');
    Route::get('/master-admin/get-data', [MAdminController::class, 'getDataAdmin'])->name('master-admin.get-data');
    Route::post('/master-admin/store', [MAdminController::class, 'store'])->name('master-admin.store');
    Route::get('/master-admin/details/{id}', [MAdminController::class, 'details'])->name('master-admin.details');
    Route::get('/master-admin/show/{id}', [MAdminController::class, 'show'])->name('master-admin.show');
    Route::post('/master-admin/update', [MAdminController::class, 'update'])->name('master-admin.update');
    Route::get('/master-admin/destroy/{id}', [MAdminController::class, 'destroy'])->name('master-admin.destroy');

    //user or pengguna
    Route::get('/dashboard-user', [DashboardUserController::class, 'index'])->name('dashboard.user');

    // master user
    Route::get('/master-user', [MUserController::class, 'index'])->name('master-user.index');
    Route::get('/master-user/get-data', [MUserController::class, 'getDataUser'])->name('master-user.get-data');
    Route::post('/master-user/store', [MUserController::class, 'store'])->name('master-user.store');
    Route::get('/master-user/show/{id}', [MUserController::class, 'show'])->name('master-user.show');
    Route::post('/master-user/update/{id}', [MUserController::class, 'update'])->name('master-user.update');
    Route::get('/master-user/destroy/{id}', [MUserController::class, 'destroy'])->name('master-user.destroy');

    // config
    Route::get('/configuration', [ConfigurationController::class, 'index'])->name('configuration');

    // Permission
    Route::get('/permission', [PermissionController::class, 'index'])->name('permission.index');
    Route::get('/permission/get-data', [PermissionController::class, 'getData'])->name('permission.get-data');
    Route::post('/permission/store', [PermissionController::class, 'store'])->name('permission.store');
    Route::get('/permission/show/{id}', [PermissionController::class, 'show'])->name('permission.show');
    Route::post('/permission/updated', [PermissionController::class, 'update'])->name('permission.update');
    Route::get('/permission/destroy/{id}', [PermissionController::class, 'destroy'])->name('permission.destroy');

    // role
    Route::get('/role', [RoleController::class, 'index'])->name('role.index');
    Route::get('/role/get-role', [RoleController::class, 'getData'])->name('role.get-data');
    Route::post('/role/store', [RoleController::class, 'store'])->name('role.store');
    Route::get('/role/show/{id}', [RoleController::class, 'show'])->name('role.show');
    Route::get('/role/show-edit/{id}', [RoleController::class, 'showedit'])->name('role.show-edit');
    Route::post('/role/update/{id}', [RoleController::class, 'update'])->name('role.update');
    Route::get('/role/destroy/{id}', [RoleController::class, 'destroy'])->name('role.destroy');

    // Logout
    Route::get('/logout', [AuthController::class, 'logout'])->name('auth.logout');
});

Route::get('/404', function(){
    abort(404);
})->name('not_found');

