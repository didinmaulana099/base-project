<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            [
                'username' => 'superadmin',
                'password' => Hash::make('01230123'),
                'full_name' => 'Superadmin',
                'api_token' => '',
                'active' => 1
            ]
        ];

        $role = Role::first();

        foreach ($datas as $data) {

            $user = User::firstOrNew(['username' => $data['username']]);
            $user->username = $data['username'];
            $user->password = $data['password'];
            $user->full_name = $data['full_name'];
            $user->api_token = null;
            $user->active = $data['active'];
            $user->created_by = 1;
            $user->updated_by = 1;
            $user->role_id = $role->id;
            $user->role_name = $role->name;
            $user->save();

        }
    }
}
