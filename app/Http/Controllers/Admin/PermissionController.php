<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Permission;

class PermissionController extends Controller
{
    public function getData(){
        $permission = Permission::with('details')->get();

        return response()->json([
            'type' => 'success',
            'data' => $permission
        ], 200);
    }

    public function index() {
        $listPermission = Permission::where('parent_id', null)->get();
        return view('settings.permission.index', [
            'list_permission' => $listPermission,
        ]);
    }

    public function store(Request $request)
    {

        $parent = Permission::find($request->parent_id);
        $permission = new Permission;
        $permission->name = $request->name;
        $permission->description = $request->description;
        $permission->url_name = $request->url_name;
        $permission->url_path = $request->url_path;
        $permission->icon = $request->icon;
        $permission->parent_id = $request->parent_id;
        $permission->parent_name = $parent->name ?? null;
        $permission->order_number = $request->order_number;
        $permission->created_by = auth()->user()->id;
        $permission->save();

        return response()->json([
            'type' => 'success',
            'message' => 'Permission Berhasil Ditambahkan!',
        ], 200);

        // return redirect()->route('permission.index');
    }

    public function show(Request $request, $id)
    {
        $permission = Permission::findOrFail($id);

        return response()->json([
            'type' => 'success',
            'data' => $permission
        ], 200);
    }

    public function update(Request $request)
    {
        $ids = $request->permission_id;
        $permission = Permission::findOrFail($ids);
        $permission->name = $request->name;
        $permission->description = $request->desc;
        $permission->url_path = $request->url_path;
        $permission->url_name = $request->url_name;
        $permission->icon = $request->icon;
        if ($request->parent_id != null) {
            $parent = Permission::where('id', $request->parent_id)->first();
            $permission->parent_id = $request->parent_id;
            $permission->parent_name = $parent->name;
        }
        $permission->order_number = $request->order_number;
        $permission->updated_by = auth()->user()->updated_by;
        $permission->save();

        return redirect()->route('permission.index');
    }

    public function destroy(Request $request)
    {
        $permission = Permission::where('id', $request->id)->first();
        $permission->delete();

        return response()->json([
            'type' => 'success',
            'message' => 'Deleted success'
        ]);
    }

    // public function list(Request $request)
    // {
    //     $permissions = Permission::when($request->keyword, function($query) use ($request) {
    //         if (!empty($request->keyword)) {
    //             $query->where('name', 'like', '%'.$request->keyword.'%');
    //         }
    //     })->take(10)
    //     ->get();

    //     return response()->json([
    //         'type' => 'success',
    //         'data' => $permissions
    //     ], 200);
    // }

    public function listParentId(Request $request)
    {
        $permissions = Permission::whereNull('parent_id')->get();

        return response()->json([
            'type' => 'success',
            'data' => $permissions
        ], 200);
    }

    public function get()
    {
        $permissions = Permission::all();

        return response()->json([
            'type' => 'success',
            'data' => $permissions
        ], 200);
    }
}


