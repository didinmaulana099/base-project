<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;
use App\Models\ProfileAdmin;
use App\Models\Role;
use App\Models\User;

class MAdminController extends Controller
{
    public function index(){
        $listRole = Role::get();
        return view('admin.master.m_admin', ['list_role' => $listRole]);
    }

    public function getDataAdmin(){
        $data = ProfileAdmin::orderBy('id', 'desc')->get();
        return response()->json([
            'data' => $data,
            'type' => 'success',
            'message' => 'get data berhasil',
        ], 200);
    }

    public function store(Request $request){
        $role = Role::where('id', $request->role)->first();
        $admin = new User;
        $admin->full_name = $request->nama_lengkap;
        $admin->username = $request->nomor_identitas;
        $admin->password = Hash::make('09910991');
        $admin->role_id = $role->id;
        $admin->role_name = $role->name;
        $admin->active = 1;
        $admin->save();

        $profilAdmin = new ProfileAdmin;
        $profilAdmin->user_id = $admin->id;
        $profilAdmin->nama_lengkap = $request->nama_lengkap;
        $profilAdmin->nomor_identitas = $request->nomor_identitas;
        $profilAdmin->email = $request->email;
        $profilAdmin->no_hp = $request->no_hp;
        $profilAdmin->active = 1;
        $profilAdmin->save();

        return redirect()->route('admin');
    }


    public function show(Request $request, $id)
    {
        $admin = ProfileAdmin::findOrFail($id);
        return view('admin.master.m_admin-details', [
            'admin' => $admin
        ]);
    }

    public function update(Request $request)
    {
        $role = Role::where('id', $request->role)->first();
        $admin = new User;
        $admin->full_name = $request->nama_lengkap;
        $admin->username = $request->nomor_identitas;
        $admin->password = Hash::make('09910991');
        $admin->role_id = $role->id;
        $admin->role_name = $role->name;
        $admin->active = 1;
        $admin->save();

        $profilAdmin = new ProfileAdmin;
        $profilAdmin->user_id = $admin->id;
        $profilAdmin->nama_lengkap = $request->nama_lengkap;
        $profilAdmin->nomor_identitas = $request->nomor_identitas;
        $profilAdmin->email = $request->email;
        $profilAdmin->no_hp = $request->no_hp;
        $profilAdmin->active = 1;
        $profilAdmin->save();

        return redirect()->route('admin');
    }


    public function details(Request $request, $id)
    {
        $admin = ProfileAdmin::findOrFail($id);
        return view('admin.master.m_admin-show', [
            'admin' => $admin
        ]);
    }


    public function destroy(Request $request)
    {
        $admin = ProfileAdmin::where('id', $request->id)->first();
        // $admin->delete();

        Alert::success('Success', 'Deledet Successfully');
        return redirect()->route('admin');
    }


}
