<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\PermissionDetails;

class Role extends Model
{
    protected $dates = [
        'deleted_at'
    ];

    protected $table = 'role';

    public function permissions()
    {
        return $this->hasMany(PermissionDetails::class, 'role_id', 'id');
    }
}
